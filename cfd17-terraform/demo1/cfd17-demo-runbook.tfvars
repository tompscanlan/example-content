# DRP Endpoint spun up by Rob - basic 'universal-runbook' workflow demo
# with 'hello-world' task and 'ansible-apply-playbook' demo task uses
# Context pool named 'contexts'

machine_count        = 2
pool                 = "contexts"
timeout              = "3m"
profiles             = [ "cfd17-demo-runbook" ]

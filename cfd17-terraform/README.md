# cfd17-terraform



## Getting started

Cloud Field Day 17 - Terraform pieces for interaction with RackN Digital
 Rebar Platform (DRP).


## DRP Content Bundle

This repo contains an example Content Bundle to apply to a DRP Endpoint to
use in conjunction with the Terraform examples.

The content bundle should be bundled and installed on the DRP Endpoint prior
to using any values from it.  See the `contents/` directory `meta.yaml`
Documentation field for more details.

The primary purpose of the Content Bundle is to deliver configuration
data Profiles to drive the desired provisioning behaviors.


## DRP Endpoint and Authorization

The DRP terraform provider requires that the DRP Endpoint be set in
Terraform configuration.  This can be done by defining the Terraform
variable `drp_endpoint`.  The `RS_ENDPOINT` shell environment variable
will be ignored.

For Authentication, use the shell environment variables or `.drpclirc`
config file.  Standard shell variables are used (eg RS_USERNAME and
RS_PASSWORD or RS_KEY).  The Terraform provider does not source or use
the `.drpclirc` config file.


## Configuring Terraform: .terraformrc and Environment Variables

To specify Auto Approve for both apply and destroy actions, set:

export TF_CLI_ARGS_apply="-auto-approve"
export TF_CLI_ARGS_destroy="-auto-approve"

Reminder, you may want to create `$HOME/.terraformrc` and place global
 configuration values to apply at all command line execution.

For more details see:

- https://developer.hashicorp.com/terraform/cli/config/config-file

For Environment Variables that can impact CLI usage, see:

- https://developer.hashicorp.com/terraform/cli/config/environment-variables


## Requesting machines

To request N machines (default is "1") from a given pool (default is "default"
pool), perform the following:

- `terraform apply -var="machine_count=2" -var="pool=contexts" -var='profiles=["cfd17-demo-runbook"]'`

Additionally, use of the `-var-file` directive can be used for different
usage scenarios staged in variable files.  In this case, an example usage
is:

- `terraform apply -var-file="cfd17-demo-photon.tfvars"`

Values in the vars file can be overriden on the command line, for example:
- `terraform apply -var-file="cfd17-demo-photon.tfvars" -var="machine_count=2"`
